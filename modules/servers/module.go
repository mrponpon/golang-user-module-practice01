package servers

import (
	middlewarehandlers "user/module/01/middlewares/middlewareHandlers"
	middlewarerepositories "user/module/01/middlewares/middlewareRepositories"
	middlewareusecases "user/module/01/middlewares/middlewareUsecases"
	"user/module/01/modules/users/userHandlers"
	"user/module/01/modules/users/userRepositories"
	"user/module/01/modules/users/userUsecases"

	"github.com/gofiber/fiber/v2"
)

type IModule interface {
	UserModule()
}

type module struct {
	router fiber.Router
	server *server
	mid    middlewarehandlers.IMiddlewareHandler
}

func InitModule(router fiber.Router, server *server, mid middlewarehandlers.IMiddlewareHandler) IModule {
	return &module{
		router: router,
		server: server,
		mid:    mid,
	}
}

func MiddlerwareModule(s *server) middlewarehandlers.IMiddlewareHandler {
	repository := middlewarerepositories.MiddlewareRepo(s.db)
	usecase := middlewareusecases.MiddlewareUsecase(repository)
	handler := middlewarehandlers.Middlewarehandler(s.cfg, usecase)
	return handler
}

func (m *module) UserModule() {
	repository := userRepositories.UserRepo(m.server.db)
	usecase := userUsecases.UserUsecase(m.server.cfg, repository)
	handler := userHandlers.Userhandler(m.server.cfg, usecase)
	router := m.router.Group("/users")
	router.Post("/signup", handler.SignUpCustomer)
	router.Post("/signup-admin", handler.SignUpAdmin)
	router.Post("/signin", handler.SignIn)
	router.Post("/signout", handler.SignOut)
	router.Post("/refresh", handler.Refresh)
}
