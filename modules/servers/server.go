package servers

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"user/module/01/config"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type IServer interface {
	Start()
}

type server struct {
	app *fiber.App
	db  *gorm.DB
	cfg config.Iconfig
}

func NewServer(cfg config.Iconfig, db *gorm.DB) IServer {
	return &server{
		cfg: cfg,
		db:  db,
		app: fiber.New(
			fiber.Config{
				AppName:      cfg.App().Name(),
				BodyLimit:    cfg.App().BodyLimit(),
				ReadTimeout:  cfg.App().ReadTimeout(),
				WriteTimeout: cfg.App().WriteTimeout(),
				JSONEncoder:  json.Marshal,
				JSONDecoder:  json.Unmarshal,
			}),
	}
}
func (s *server) Start() {
	middlerware := MiddlerwareModule(s)
	s.app.Use(middlerware.Cors())
	v1 := s.app.Group("v1")
	module := InitModule(v1, s, middlerware)
	module.UserModule()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		_ = <-c
		log.Println("server is shutting down..")
		_ = s.app.Shutdown()

	}()
	log.Printf("server is starting on %v", s.cfg.App().Url())
	s.app.Listen(s.cfg.App().Url())

}
