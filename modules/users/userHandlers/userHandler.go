package userHandlers

import (
	"user/module/01/config"
	"user/module/01/entities"
	"user/module/01/modules/users"
	"user/module/01/modules/users/userUsecases"

	"github.com/gofiber/fiber/v2"
)

type IUserHandler interface {
	SignUpCustomer(c *fiber.Ctx) error
	SignUpAdmin(c *fiber.Ctx) error
	SignIn(c *fiber.Ctx) error
	SignOut(c *fiber.Ctx) error
	Refresh(c *fiber.Ctx) error
}

type userhandler struct {
	cfg         config.Iconfig
	userusecase userUsecases.IUserUsecase
}

func Userhandler(cfg config.Iconfig, userusecase userUsecases.IUserUsecase) IUserHandler {
	return &userhandler{
		cfg:         cfg,
		userusecase: userusecase,
	}
}

func (h *userhandler) SignUpCustomer(c *fiber.Ctx) error {
	req := new(users.UserRegisterReq)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}

	if !req.IsEmail() {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			"email pattern is invalid",
		).Res()
	}
	result, err := h.userusecase.InsertCustomer(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}

	return entities.NewResponse(c).Success(fiber.StatusCreated, result).Res()

}

func (h *userhandler) SignUpAdmin(c *fiber.Ctx) error {
	req := new(users.UserRegisterReq)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}

	if !req.IsEmail() {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			"email pattern is invalid",
		).Res()
	}
	result, err := h.userusecase.InsertAdmin(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}

	return entities.NewResponse(c).Success(fiber.StatusCreated, result).Res()

}

func (h *userhandler) SignIn(c *fiber.Ctx) error {
	req := new(users.UserCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}
	passport, err := h.userusecase.GetPassport(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}

	return entities.NewResponse(c).Success(fiber.StatusOK, passport).Res()
}

func (h *userhandler) SignOut(c *fiber.Ctx) error {
	req := new(users.UserRemoveCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}
	id, err := h.userusecase.RemoveOauthId(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, id).Res()
}

func (h *userhandler) Refresh(c *fiber.Ctx) error {
	req := new(users.UserRefreshCredential)
	err := c.BodyParser(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}
	passport, err := h.userusecase.RefreshPassport(req)
	if err != nil {
		return entities.NewResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}
	return entities.NewResponse(c).Success(fiber.StatusCreated, passport).Res()
}
