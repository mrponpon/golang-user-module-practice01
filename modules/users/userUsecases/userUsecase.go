package userUsecases

import (
	"fmt"
	"user/module/01/config"
	"user/module/01/modules/users"
	"user/module/01/modules/users/userRepositories"
	"user/module/01/pkg/auth"

	"golang.org/x/crypto/bcrypt"
)

type IUserUsecase interface {
	InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error)
	InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error)
	GetPassport(req *users.UserCredential) (*users.UserPassport, error)
	RemoveOauthId(req *users.UserRemoveCredential) (string, error)
	RefreshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error)
}

type userUsecase struct {
	cfg            config.Iconfig
	userrepository userRepositories.IUserRepo
}

func UserUsecase(cfg config.Iconfig, userrepository userRepositories.IUserRepo) IUserUsecase {
	return &userUsecase{
		cfg:            cfg,
		userrepository: userrepository,
	}
}

func (u *userUsecase) InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error) {
	//Hash password
	if err := req.BcryptHashing(); err != nil {
		return nil, err
	}
	//Insert user
	result, err := u.userrepository.InsertUser(req, false)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userUsecase) InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error) {
	//Hash password
	if err := req.BcryptHashing(); err != nil {
		return nil, err
	}
	//Insert user
	result, err := u.userrepository.InsertUser(req, true)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (u *userUsecase) GetPassport(req *users.UserCredential) (*users.UserPassport, error) {
	user, err := u.userrepository.FindOneUserByEmail(req.Email)
	if err != nil {
		return nil, err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return nil, fmt.Errorf("password is invalid")
	}
	userclaim := &(users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	})
	accessToken, err := auth.NewAuth("access", u.cfg.Jwt(), userclaim)
	refreshToken, err := auth.NewAuth("refresh", u.cfg.Jwt(), userclaim)
	userpassport := &(users.UserPassport{
		User: &users.User{
			Id:       user.Id,
			Email:    user.Email,
			Username: user.Username,
			RoleId:   user.RoleId,
		},
		Token: &users.UserToken{
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refreshToken.SignToken(),
		},
	})
	id, err := u.userrepository.InsertOauth(userpassport)
	userpassport.Token.Id = id
	return userpassport, nil
}

func (u *userUsecase) RemoveOauthId(req *users.UserRemoveCredential) (string, error) {
	id, err := u.userrepository.DeleteOauth(req)
	if err != nil {
		return "", err
	}
	return id, nil
}

func (u *userUsecase) RefreshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error) {
	claims, err := auth.ParseToken(u.cfg.Jwt(), req.RefreshToken)
	fmt.Println(claims.Claim)
	if err != nil {
		return nil, err
	}
	oauthid, err := u.userrepository.FindOneOauth(req)
	if err != nil {
		return nil, err
	}
	user, err := u.userrepository.GetUserbyUserid(oauthid.UserId)
	if err != nil {
		return nil, err
	}
	newClaims := &users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	}
	accessToken, err := auth.NewAuth("access", u.cfg.Jwt(), newClaims)
	if err != nil {
		return nil, err
	}
	refresh_token := auth.RepeatToken(u.cfg.Jwt(), newClaims, claims.ExpiresAt.Unix())
	userpassport := &(users.UserPassport{
		User: user,
		Token: &users.UserToken{
			Id:           oauthid.Id,
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refresh_token,
		},
	})
	err = u.userrepository.UpdateOauth(userpassport.Token)
	if err != nil {
		return nil, err
	}
	return userpassport, nil
}
