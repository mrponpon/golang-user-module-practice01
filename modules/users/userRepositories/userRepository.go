package userRepositories

import (
	"fmt"
	"user/module/01/modules/users"

	"gorm.io/gorm"
)

type IUserRepo interface {
	InsertUser(req *users.UserRegisterReq, isAdmin bool) (*users.UserPassport, error)
	FindOneUserByEmail(email string) (*users.UserCredentialCheck, error)
	InsertOauth(req *users.UserPassport) (string, error)
	DeleteOauth(req *users.UserRemoveCredential) (string, error)
	UpdateOauth(req *users.UserToken) error
	FindOneOauth(req *users.UserRefreshCredential) (*users.OauthId, error)
	GetUserbyUserid(id string) (*users.User, error)
}

type userRepo struct {
	db *gorm.DB
}

func UserRepo(db *gorm.DB) IUserRepo {
	return &userRepo{
		db: db,
	}
}

func (r *userRepo) InsertUser(req *users.UserRegisterReq, isAdmin bool) (*users.UserPassport, error) {
	var id string
	if isAdmin {
		query := `
		INSERT INTO "users" (
			"email",
			"password",
			"username",
			"role_id"
		)
		VALUES
			($1, $2, $3, 2)
		RETURNING "id";`
		err := r.db.Raw(query, req.Email, req.Password, req.Username).Scan(&id).Error
		if err != nil {
			switch err.Error() {
			case "ERROR: duplicate key value violates unique constraint \"users_username_key\" (SQLSTATE 23505)":
				return nil, fmt.Errorf("username has been used")
			case "ERROR: duplicate key value violates unique constraint \"users_email_key\" (SQLSTATE 23505)":
				return nil, fmt.Errorf("email has been used")
			default:
				return nil, fmt.Errorf("insert user failed: %v", err)
			}
		}

	} else {
		query := `
		INSERT INTO "users" (
			"email",
			"password",
			"username",
			"role_id"
		)
		VALUES
			($1, $2, $3, 1)
		RETURNING "id";`
		err := r.db.Raw(query, req.Email, req.Password, req.Username).Scan(&id).Error
		if err != nil {
			switch err.Error() {
			case "ERROR: duplicate key value violates unique constraint \"users_username_key\" (SQLSTATE 23505)":
				return nil, fmt.Errorf("username has been used")
			case "ERROR: duplicate key value violates unique constraint \"users_email_key\" (SQLSTATE 23505)":
				return nil, fmt.Errorf("email has been used")
			default:
				return nil, fmt.Errorf("insert user failed: %v", err)
			}
		}
	}
	query := `
	SELECT
		"u"."id",
		"u"."email",
		"u"."username",
		"u"."role_id"
	FROM "users" "u"
	WHERE "u"."id" = $1;`
	var user users.User
	err := r.db.Raw(query, id).Scan(&user).Error
	if err != nil {
		return nil, fmt.Errorf("get user failed: %v", err)
	}
	userpassport := new(users.UserPassport)
	userpassport.User = &user
	userpassport.Token = nil

	return userpassport, nil
}

func (r *userRepo) FindOneUserByEmail(email string) (*users.UserCredentialCheck, error) {
	query := `
		SELECT
			"id",
			"email",
			"password",
			"username",
			"role_id"
		FROM "users"
		WHERE "email" = $1;`
	user := new(users.UserCredentialCheck)
	err := r.db.Raw(query, email).Scan(&user).Error
	if err != nil {
		return nil, fmt.Errorf("fine user failed: %v", err)
	}
	return user, nil
}

func (r *userRepo) InsertOauth(req *users.UserPassport) (string, error) {
	query := `
		INSERT INTO "oauth" (
			"user_id",
			"refresh_token",
			"access_token"
		)
		VALUES ($1, $2, $3)
			RETURNING "id";`
	var id string
	err := r.db.Raw(query, req.User.Id, req.Token.RefreshToken, req.Token.AccessToken).Scan(&id).Error
	if err != nil {
		return "", fmt.Errorf("insert oauth failed: %v", err)
	}
	return id, nil
}

func (r *userRepo) DeleteOauth(req *users.UserRemoveCredential) (string, error) {
	var id string
	query := `DELETE FROM "oauth" WHERE id=$1 RETURNING "id";`
	err := r.db.Raw(query, req.OauthId).Scan(&id).Error
	if err != nil {
		return "", fmt.Errorf("delete oauth failed: %v", err)
	}
	return id, nil
}

func (r *userRepo) UpdateOauth(req *users.UserToken) error {
	query := `
	UPDATE "oauth" SET
		"access_token" = $1,
		"refresh_token" = $2
	WHERE "id" = $3;`
	err := r.db.Exec(query, req.AccessToken, req.RefreshToken, req.Id).Error
	if err != nil {
		return fmt.Errorf("update oauth failed: %v", err)

	}
	return nil

}

func (r *userRepo) FindOneOauth(req *users.UserRefreshCredential) (*users.OauthId, error) {
	query := `
		SELECT 
			"id",
			"user_id"
		FROM "oauth"
		WHERE "refresh_token" = $1;`
	useroauth := new(users.OauthId)
	err := r.db.Raw(query, req.RefreshToken).Scan(&useroauth).Error
	if err != nil {
		return nil, fmt.Errorf("find oauth failed:%v", err)
	}
	return useroauth, nil
}

func (r *userRepo) GetUserbyUserid(id string) (*users.User, error) {
	query := `
	SELECT
		"id",
		"email",
		"username",
		"role_id"
	FROM "users"
	WHERE "id" = $1;`
	user := new(users.User)
	err := r.db.Raw(query, id).Scan(&user).Error
	if err != nil {
		return nil, fmt.Errorf("get user by id failed:%v", err)
	}

	return user, nil
}
