package middlewarehandlers

import (
	"user/module/01/config"
	middlewareusecases "user/module/01/middlewares/middlewareUsecases"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

type IMiddlewareHandler interface {
	Cors() fiber.Handler
}

type middlewarehandler struct {
	cfg               config.Iconfig
	middlewareusecase middlewareusecases.IMiddlerwareUsecase
}

func Middlewarehandler(cfg config.Iconfig, middlewareusecase middlewareusecases.IMiddlerwareUsecase) IMiddlewareHandler {
	return &middlewarehandler{
		cfg:               cfg,
		middlewareusecase: middlewareusecase,
	}
}

func (h *middlewarehandler) Cors() fiber.Handler {
	return cors.New(cors.Config{
		Next:             cors.ConfigDefault.Next,
		AllowOrigins:     "*",
		AllowMethods:     "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders:     "",
		AllowCredentials: false,
		ExposeHeaders:    "",
		MaxAge:           0,
	})
}
