package middlewareusecases

import middlewarerepositories "user/module/01/middlewares/middlewareRepositories"

type IMiddlerwareUsecase interface {
}

type middlewareUsecase struct {
	middlewareRepo middlewarerepositories.IMiddlewareRepo
}

func MiddlewareUsecase(middlewareRepo middlewarerepositories.IMiddlewareRepo) IMiddlerwareUsecase {
	return &middlewareUsecase{
		middlewareRepo: middlewareRepo,
	}
}
