package middlewarerepositories

import "gorm.io/gorm"

type IMiddlewareRepo interface {
}

type middlewareRepo struct {
	db *gorm.DB
}

func MiddlewareRepo(db *gorm.DB) IMiddlewareRepo {
	return &middlewareRepo{
		db: db,
	}
}
