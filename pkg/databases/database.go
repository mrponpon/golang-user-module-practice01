package databases

import (
	"fmt"
	"log"
	"user/module/01/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func DbConnect(cfg config.IDbconfig) *gorm.DB {
	//connect
	db, err := gorm.Open(postgres.Open(cfg.Url()), &gorm.Config{})
	if err != nil {
		log.Fatalf("connect db failed: %v\n", err)
	}
	fmt.Println("connect db successfully")
	return db
}
