package logger

import (
	"log"
	"time"
	"user/module/01/utils"

	"github.com/gofiber/fiber/v2"
)

type ILogger interface {
	Print() ILogger
	SetQuery(c *fiber.Ctx)
	SetBody(c *fiber.Ctx)
	SetResponse(res any)
}

type eshoplogger struct {
	Time       string `json:"time"`
	Ip         string `json:"ip"`
	Method     string `json:"method"`
	StatusCode int    `json:"status_code"`
	Path       string `json:"path"`
	Query      any    `json:"query"`
	Body       any    `json:"body"`
	Response   any    `json:"response"`
}

func (l *eshoplogger) Print() ILogger {
	utils.Debug(l)
	return l
}

func (l *eshoplogger) SetQuery(c *fiber.Ctx) {
	var body any
	if err := c.QueryParser(&body); err != nil {
		log.Printf("query parser error: %v", err)
	}
	l.Query = body
}

func (l *eshoplogger) SetBody(c *fiber.Ctx) {
	var body any
	if err := c.BodyParser(&body); err != nil {
		log.Printf("body parser error: %v", err)
	}
	if l.Path == "v1/users/signup" {
		l.Body = ""
	} else {
		l.Body = body
	}
}
func (l *eshoplogger) SetResponse(res any) {
	l.Response = res
}

func NewLogger(c *fiber.Ctx, res any) ILogger {
	log := &eshoplogger{
		Time:       time.Now().Local().Format("2006-01-02 15:04:05"),
		Ip:         c.IP(),
		Method:     c.Method(),
		Path:       c.Path(),
		StatusCode: c.Response().StatusCode(),
	}
	log.SetQuery(c)
	log.SetBody(c)
	log.SetResponse(res)
	return log
}
