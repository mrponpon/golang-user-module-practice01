package auth

import (
	"errors"
	"fmt"
	"math"
	"time"
	"user/module/01/config"
	"user/module/01/modules/users"

	"github.com/golang-jwt/jwt/v5"
)

type IAuth interface {
	SignToken() string
}

type auth struct {
	mapclaim *mapclaims
	cfg      config.IJwtconfig
}

type mapclaims struct {
	Claim *users.UserClaims
	jwt.RegisteredClaims
}

func NewAuth(tokentype string, cfg config.IJwtconfig, claim *users.UserClaims) (IAuth, error) {
	if tokentype == "access" {
		return newAccessToken(cfg, claim), nil
	} else if tokentype == "admin" {
		return newAdminToken(cfg), nil
	} else if tokentype == "refresh" {
		return newRefreshToken(cfg, claim), nil
	} else {
		return nil, fmt.Errorf("unknow token type")
	}
}

func newAccessToken(cfg config.IJwtconfig, claims *users.UserClaims) IAuth {
	return &auth{
		cfg: cfg,
		mapclaim: &mapclaims{
			Claim: claims,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "eshop-api",
				Subject:   "access-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeDurationCal(cfg.AccessExpiresAt()),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
}

func newRefreshToken(cfg config.IJwtconfig, claims *users.UserClaims) IAuth {
	return &auth{
		cfg: cfg,
		mapclaim: &mapclaims{
			Claim: claims,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "eshop-api",
				Subject:   "refresh-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeDurationCal(cfg.RefreshExpiresAt()),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
}

func newAdminToken(cfg config.IJwtconfig) IAuth {
	return &auth{
		cfg: cfg,
		mapclaim: &mapclaims{
			Claim: nil,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "eshop-api",
				Subject:   "admin-token",
				Audience:  []string{"admin"},
				ExpiresAt: jwtTimeDurationCal(600),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
}

func (a *auth) SignToken() string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, a.mapclaim)
	ss, _ := token.SignedString(a.cfg.SecretKey())
	return ss

}

func jwtTimeDurationCal(t int) *jwt.NumericDate {
	return jwt.NewNumericDate(time.Now().Add(time.Duration(int64(t) * int64(math.Pow10(9)))))
}
func jwtTimeRepeatAdapter(t int64) *jwt.NumericDate {
	return jwt.NewNumericDate(time.Unix(t, 0))
}

func ParseToken(cfg config.IJwtconfig, tokenString string) (*mapclaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &mapclaims{}, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method is invalid")

		}
		return cfg.SecretKey(), nil
	})
	if err != nil {
		if errors.Is(err, jwt.ErrTokenMalformed) {
			return nil, fmt.Errorf("token format is invalid")
		} else if errors.Is(err, jwt.ErrTokenExpired) {
			return nil, fmt.Errorf("token had expired")

		} else {
			return nil, fmt.Errorf("parse token failed: %v", err)
		}
	}

	claims, ok := token.Claims.(*mapclaims)
	if ok {
		return claims, nil
	} else {
		return nil, fmt.Errorf("claim type is invalid")
	}
}

func RepeatToken(cfg config.IJwtconfig, claims *users.UserClaims, exp int64) string {
	obj := &auth{
		cfg: cfg,
		mapclaim: &mapclaims{
			Claim: claims,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "eshop-api",
				Subject:   "refresh-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeRepeatAdapter(exp),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
	return obj.SignToken()
}
