package entities

import (
	"user/module/01/pkg/logger"

	"github.com/gofiber/fiber/v2"
)

type IResponse interface {
	Success(code int, data any) IResponse
	Error(code int, msg string) IResponse
	Res() error
}

type Response struct {
	StatusCode int
	Data       any
	ErrorRes   *ErrorResponse
	Context    *fiber.Ctx
	IsError    bool
}

type ErrorResponse struct {
	Msg string `json:"message"`
}

func NewResponse(c *fiber.Ctx) IResponse {
	return &Response{
		Context: c,
	}
}
func (r *Response) Success(code int, data any) IResponse {
	r.StatusCode = code
	r.Data = data
	logger.NewLogger(r.Context, &r.Data).Print()
	return r
}
func (r *Response) Error(code int, msg string) IResponse {
	r.StatusCode = code
	r.ErrorRes = &ErrorResponse{
		Msg: msg,
	}
	r.IsError = true
	logger.NewLogger(r.Context, &r.ErrorRes).Print()

	return r
}
func (r *Response) Res() error {
	return r.Context.Status(r.StatusCode).JSON(func() any {
		if r.IsError {
			return &r.ErrorRes
		}
		return &r.Data
	}())
}
