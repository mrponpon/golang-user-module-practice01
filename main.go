package main

import (
	_ "fmt"
	"log"
	"os"
	"user/module/01/config"
	"user/module/01/modules/servers"
	"user/module/01/pkg/databases"
)

func envPath() string {
	if len(os.Args) == 1 {
		return ".env"
	} else {
		return os.Args[1]
	}
}

func main() {
	cfg := config.LoadConfig(envPath())
	db := databases.DbConnect(cfg.Db())
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("sql db failed: %v\n", err)
	}
	sqlDB.SetMaxOpenConns(cfg.Db().MaxConnections())
	defer sqlDB.Close()
	servers.NewServer(cfg, db).Start()
}
